import random
import math
import harfang as hg

#----------------------------- GLOBALS --------------------------------------------------------------------------------
key_node = None
player_light_node = None
cpt_after_light = 0
#-----------------------------------------------------------------------------------------------------------------------


def GetNextCaseIdxInRoad(coord, road):  # Get the next casein a road if exist
	for idx, case in enumerate(road):
		case_coord = case.coord
		if [coord[0] + 1, coord[1]] == case_coord or [coord[0] - 1, coord[1]] == case_coord or \
			[coord[0], coord[1] + 1] == case_coord or [coord[0], coord[1] - 1] == case_coord:
			return idx
	return None


def GetNeighStartRoadCoord(case_coord, roads):  # Get neighbors of a main road case in an other road
	neighbors_coord = []

	for road_idx in range(1, len(roads)):  # Browse all roads exept the Main
		road = roads[road_idx]

		neigh_case_idx = GetNextCaseIdxInRoad(case_coord, road)  # Get potencial neighbor

		if neigh_case_idx != None:
			neigh_case_coord = road[neigh_case_idx].coord
			neighbors_coord.append(neigh_case_coord)  # If it exist, add it

	return neighbors_coord


def GetPrevDir(actual_case_coord, prev_case_coord):  # Get direction between a case and the previous case
	return [actual_case_coord[0] - prev_case_coord[0], actual_case_coord[1] - prev_case_coord[1]]


def GetNextDir(actual_case_coord, next_case_coord):  # Get direction between a case and the next case
	return [next_case_coord[0] - actual_case_coord[0], next_case_coord[1] - actual_case_coord[1]]


def GetImpasseRoadTypeAndDirection(case_idx, road_idx, roads):  # Get the parameters of the end case in a road
	# Check if the is on the main road or not
	if case_idx == 0:
		case_coord = roads[road_idx][case_idx].coord
		dir = GetNextDir(case_coord, roads[road_idx][1].coord)
		road_type = "path_end"
	else:
		case_coord = roads[road_idx][case_idx].coord
		dir = GetPrevDir(case_coord, roads[road_idx][-2].coord)
		if road_idx == 0:
			road_type = "path_end"
			dir = [-dir[0], -dir[1]]
		else:
			road_type = "pathA" + "_deadend"

	# Get the corresponding rotation angle for the 3D model
	if dir == [0, 1]:
		angle = math.radians(90)
	elif dir == [0, -1]:
		angle = math.radians(-90)
	elif dir == [1, 0]:
		angle = math.radians(180)
	else:
		angle = math.radians(0)

	# Create the matrix of the road
	road_matrix = hg.Matrix4.TransformationMatrix(hg.Vector3(case_coord[0] * 2, 1.1, case_coord[1] * 2),
													 hg.Vector3(0, angle, 0))
	return road_type, road_matrix


def GetTRoadDirection(case_idx, road, neighbor_coord):  # Get the direction of a "T-form" road
	# Always situated on the main road, when an annexe road start at the middle of three cases aligned of the main road
	
	road_annex_case_coord = neighbor_coord  # Coordinates of the start case of the road annexe

	main_road_case_coord = road[case_idx].coord  # Coordinates of the next main road case of the road_annex_case_coord, (the base case)

	main_to_road_dir = GetPrevDir(road_annex_case_coord, main_road_case_coord)  # Direction from the main road case to the annexe road case

	base_prev_dir = GetPrevDir(main_road_case_coord, road[case_idx - 1].coord)  # Direction from the main road case to the previous main road case
	base_next_dir = GetNextDir(main_road_case_coord, road[case_idx + 1].coord)  # Direction from the main road case to the next main road case
	inverted_base_next_dir = GetNextDir(road[case_idx + 1].coord, main_road_case_coord)  # Inverse of the base_next_dir
	inverted_base_prev_dir = GetPrevDir(road[case_idx - 1].coord, main_road_case_coord)  # Inverse of the base_prev_dir

	if base_prev_dir == base_next_dir:  # If the three cases on the main road are aligned
		return main_to_road_dir  # Direction for the 3D model is main_to_road_dir

	else:  # If the three cases on the main are in angle
		if base_prev_dir == main_to_road_dir:  # If the main road case is aligned with the previous case (on main road) and the start case of the road annexe
			return base_next_dir
		elif inverted_base_next_dir == main_to_road_dir: # If the main road case is aligned with the next case (on main road) and the start case of the road annexe
			return inverted_base_prev_dir


def GetAnnexRoadBaseTypeAndDirection(case_idx, road, neighbors_coord):  # Get parameters of a case on the main road with one or two road(s) annexe

	if len(neighbors_coord) > 1:  # If the case have more than 1 neighbor (2 annexes roads)
		road_type = "pathA" + random.choice(["", "_broke"]) + "_quadform"

		dir = GetPrevDir(road[case_idx].coord, road[case_idx - 1].coord)

		if dir == [0, 1]: angle = math.radians(90)
		elif dir == [0, -1]: angle = math.radians(-90)
		elif dir == [1, 0]: angle = math.radians(180)
		else: angle = math.radians(0)

	else:  # If the case have only 1 neighbor (1 annexes roads)
		road_type = "pathA" + random.choice(["", "_broke"]) + "_tform"
		neighbor_coord = neighbors_coord[0]

		dir_T = GetTRoadDirection(case_idx, road, neighbor_coord)  # Get the T-form model direction
		# Get the corresponding rotation angle for the 3D model
		if dir_T == [0, 1]: angle = math.radians(180)
		elif dir_T == [0, -1]: angle = math.radians(0)
		elif dir_T == [1, 0]: angle = math.radians(-90)
		else: angle = math.radians(90)

	case_coord = road[case_idx].coord
	# Create the matrix of the road
	road_matrix = hg.Matrix4.TransformationMatrix(hg.Vector3(case_coord[0] * 2, 1.1, case_coord[1] * 2),
												 hg.Vector3(0, angle, 0))
	return road_type, road_matrix


def GetRoadTypeAndDirection(case_idx, road_idx, roads):  # Get parameters of a case corresponding to a normal road (without annexe road)
	road = roads[road_idx]
	road_type = "path"
	angle = 0
	case_coord = road[case_idx].coord

	# Get the direction between the base case and the next case
	next_case_coord = road[case_idx + 1].coord
	next_dir = GetNextDir(case_coord, next_case_coord)

	# Define the previous case (if the base case is the first of the road)
	if case_idx == 0:
		prev_case_in_main_road = roads[0][GetNextCaseIdxInRoad(road[case_idx].coord, roads[0])]
		prev_case_coord = prev_case_in_main_road.coord
	else:
		prev_case_coord = road[case_idx - 1].coord

	# Get the direction between the base case and the previous case
	prev_dir = GetPrevDir(case_coord, prev_case_coord)

	# Determine the direction
	if prev_dir == next_dir:  # If the two directions are the same ( and the three cases are aligned)
		road_type += 'A' + random.choice(["", "_broke"])
		if prev_dir[1] != 0:
			angle = 90  # Vertical road
		else:
			angle = 0  # Horizontale road
	else:  # If the two directions are different ( and the three cases are in angle)
		road_type += "_turn"
		if (prev_dir == [0, -1] and next_dir == [1, 0]) or (prev_dir == [-1, 0] and next_dir == [0, 1]):
			angle = 180
		elif (prev_dir == [1, 0] and next_dir == [0, 1]) or (prev_dir == [0, -1] and next_dir == [-1, 0]):
			angle = 90
		elif (prev_dir == [1, 0] and next_dir == [0, -1]) or (prev_dir == [0, 1] and next_dir == [-1, 0]):
			angle = 0
		elif (prev_dir == [0, 1] and next_dir == [1, 0]) or (prev_dir == [-1, 0] and next_dir == [0, -1]):
			angle = -90

	# Create the matrix of the road
	road_matrix = hg.Matrix4.TransformationMatrix(hg.Vector3(case_coord[0] * 2, 1.1, case_coord[1] * 2),
												  hg.Vector3(0, math.radians(angle), 0))
	return road_type, road_matrix


def CreateLabyModel(roads, scn, plus):  # Create the full labyrinth
	global cpt_after_light, start_case_in_main_road, key_node, traps_node, player_light_node

	geo_path = "assets/laby/"
	geo_extention = ".geo"

	for road_idx, road in enumerate(roads):
		for case_idx, case in enumerate(road):

			# ROAD
			# Get possible neighbors if the case is on the main road and is not the first
			if road_idx == 0:
				neighbors_coord = GetNeighStartRoadCoord(roads[road_idx][case_idx].coord, roads)
			else:
				neighbors_coord = []

			# If the case is the end of a road or the start case of the main road
			if case_idx == len(road) - 1 or (road_idx == 0 and case_idx == 0):
				road_type, road_matrix = GetImpasseRoadTypeAndDirection(case_idx, road_idx, roads)  # Get the parameters
				road_path = geo_path + road_type + geo_extention  # Create the path of the geometry
				plus.AddGeometry(scn, road_path, road_matrix)  # Add the geometry

				if road_type == "path_end":
					if case_idx == 0:
						print("add red light")
						plus.AddLight(scn, hg.Matrix4.TranslationMatrix(road_matrix.GetTranslation()),
									hg.LightModelPoint, 1.5, False, hg.Color(2, 1, 1))
					else:
						print("add green light")
						plus.AddLight(scn, hg.Matrix4.TranslationMatrix(road_matrix.GetTranslation()),
									hg.LightModelPoint, 1.5, False, hg.Color(1, 2, 1))


			# If case have neighbor(s) (and is on the main road)
			elif len(neighbors_coord) > 0:  # T PATH
				road_type, road_matrix = GetAnnexRoadBaseTypeAndDirection(case_idx, roads[0], neighbors_coord)
				road_path = geo_path + road_type + geo_extention
				plus.AddGeometry(scn, road_path, road_matrix)

			# If the case have no specifities (corresponding to a normal road)
			else:
				road_type, road_matrix = GetRoadTypeAndDirection(case_idx, road_idx, roads)
				road_path = geo_path + road_type + geo_extention
				plus.AddGeometry(scn, road_path, road_matrix)

				# Add light regularly
				cpt_after_light += 1
				if cpt_after_light > 6:
					plus.AddLight(scn, hg.Matrix4.TranslationMatrix(road_matrix.GetTranslation()), hg.LightModelPoint, 4, False, hg.Color(4, 3, 1))
					cpt_after_light = 0

			# TRAP AND OBJECT
			if case.trap != None:
				road_path = geo_path + case.trap.type + geo_extention
				plus.AddGeometry(scn, road_path, road_matrix)

			if "key" in case.object:
				road_path = geo_path + "key" + geo_extention
				key_node = plus.AddGeometry(scn, road_path, road_matrix)

	# Add a light above the player
	player_light_node = plus.AddLight(scn, hg.Matrix4.TranslationMatrix(hg.Vector3(0, 2, 0)), hg.LightModelPoint, 3, False, hg.Color(4, 3, 1))


def GetKeyNode():
	global key_node
	return key_node

def GetPlayerLightNode():
	global player_light_node
	return player_light_node