import random
from PIL import Image, ImageDraw

class Laby:
	def __init__(self):
		self.roads = []

class Case:

	def __init__(self, coord):
		self.coord = coord
		self.trap = None
		self.object = []

	def setCase(self, trap, objects):
		self.trap = trap
		self.object = objects

	def hasKey(self):
		return "key" in self.object

	def hasTrap(self):
		return self.trap != None

	def getTrap(self):
		return self.trap

	def trapActivated(self):
		return self.trap.isActivate()

	def isExit(self):
		return "exit" in self.object

class Trap:

	def __init__(self, id, type, dexterity, intelligence, rapidity):
		self.id = id
		self.type = type
		self.dexterity = dexterity
		self.intelligence = intelligence
		self.rapidity = rapidity

		self.activated = True

	def getType(self):
		return self.type

	def getDexterity(self):
		return self.dexterity

	def getIntelligence(self):
		return self.intelligence

	def getRapidity(self):
		return self.rapidity

	def desactivate(self):
		self.activated = False

	def isActivate(self):
		return self.activated


directions = [[0, -1], [0, 1], [1, 0], [-1, 0]]  # NORTH / SOUTH / EAST / WEST
# -------------------------- COLORS ------------------------------------------------------------------------------------
RoadColor = (0, 0, 0)
StakeTrapColor = (200, 30, 30)
ArrowTrapColor = (255, 128, 0)
StoneBallTrapColor = (96, 96, 96)
PlayerColor = (0, 153, 0)
WallColor = (255, 255, 255)
KeyColor = (255, 255, 102)
ExitColor = (102, 51, 0)

# ------------------- TRAP ---------------------------------------------------------------------------------------------
ArrowTrap = "Arrow"
StoneBallTrap = "Ball"
StakeTrap = "Stake"

nbr_echec_road = 0

def AddStakeTrap(id):# pieu
	return Trap(id, StakeTrap, 2, 6, 2)


def AddArrowTrap(id):
	return Trap(id, ArrowTrap, 4, 0, 6)


def AddStoneBallTrap(id):
	return Trap(id, StoneBallTrap, 6, 0, 4)


def AddTraps(labyCfg, add_trap_func, max_trap, min_trap):
	trap_nbr = random.randint(min_trap, max_trap)

	for x in range(trap_nbr):

		rand_case = None
		is_place = False

		while not is_place:
			is_place = True

			rand_case = GetRandomCase(labyCfg.roads)
			cases_neigh = [IsExist(labyCfg.roads, rand_case.coord[0], rand_case.coord[1] + 1),
							IsExist(labyCfg.roads, rand_case.coord[0], rand_case.coord[1] - 1),
							IsExist(labyCfg.roads, rand_case.coord[0] + 1, rand_case.coord[1]),
							IsExist(labyCfg.roads, rand_case.coord[0] - 1, rand_case.coord[1]),
						   	IsExist(labyCfg.roads, rand_case.coord[0], rand_case.coord[1])]

			for case in cases_neigh:
				if case is not None:
					if case.trap is not None or case.coord == [0, 0] or "key" in case.object:
						is_place = False

		rand_case.trap = add_trap_func(x)


def GetTrapColor(trap):
	if trap.type == ArrowTrap:
		return ArrowTrapColor
	elif trap.type == StoneBallTrap:
		return StoneBallTrapColor
	elif trap.type == StakeTrap:
		return StakeTrapColor
	else:
		return RoadColor


def InitTraps(labyCfg, max_trap, min_trap):
	AddTraps(labyCfg, AddStakeTrap, max_trap, min_trap)
	AddTraps(labyCfg, AddArrowTrap, max_trap, min_trap)
	AddTraps(labyCfg, AddStoneBallTrap, max_trap, min_trap)

#----------------------- OBJECT ----------------------------------------------------------------------------------------
def AddKey(labyCfg):
	is_place = False
	while not is_place:
		key_road = GetRandomRoad(labyCfg.roads)
		if len(key_road) > 3:
			key_road[-1].object.append("key")
			is_place = True


def InitExit(labyCfg):
	labyCfg.roads[0][-1].object.append("exit")


def IsExist(roads, x, y):
	for road in roads:
		for case in road:
			if case.coord == [x, y]:
				return case
	return None


#--------------------------- RANDOM ------------------------------------------------------------------------------------
def GetRandomCase(roads):
	rand_road_idx = random.randint(0, len(roads) - 1)
	rand_case_idx = random.randint(0, len(roads[rand_road_idx]) - 1)
	rand_case = roads[rand_road_idx][rand_case_idx]
	return rand_case


def GetRandomRoad(roads):
	rand_road_idx = random.randint(1, len(roads) - 1)
	rand_road = roads[rand_road_idx]
	return rand_road


def GetRandomRoadStartPoint(point_list):
	return random.choice(point_list[1:-1]).coord


def GetRandomDirectionOrder(nbr): # nbr of directions
	dir_order = [x for x in range(nbr)]
	for x in range(nbr):
		a = random.randint(0, nbr - 1)
		b = random.randint(0, nbr - 1)
		tmp = dir_order[a]
		dir_order[a] = dir_order[b]
		dir_order[b] = tmp

	return dir_order

#------------------------------ CREATE LABY ----------------------------------------------------------------------------
def CanBeRoad(roads, x, y, road_dir):
	if IsExist(roads, x, y):
		return False
	if (IsExist(roads, x, y - 1) is not None) and (IsExist(roads, x + 1, y - 1) is not None) and (IsExist(roads, x + 1, y) is not None):  # N / NE / E
		return False
	if (IsExist(roads, x + 1, y) is not None) and (IsExist(roads, x + 1, y + 1) is not None) and (IsExist(roads, x, y + 1) is not None):  # E / SE / S
		return False
	if (IsExist(roads, x, y + 1) is not None) and (IsExist(roads, x - 1, y + 1) is not None) and (IsExist(roads, x - 1, y) is not None):  # S / SO / O
		return False
	if (IsExist(roads, x - 1, y) is not None) and (IsExist(roads, x - 1, y - 1) is not None) and (IsExist(roads, x, y - 1) is not None):  # O / NO / N
		return False
	# if the road will meet an other
	if road_dir[0] != 0:
		if IsExist(roads, x + road_dir[0], y + road_dir[1] + 1) is not None:
			return False
		if IsExist(roads, x + road_dir[0], y + road_dir[1]) is not None:
			return False
		if IsExist(roads, x + road_dir[0], y + road_dir[1] - 1) is not None:
			return False
	else:
		if IsExist(roads, x + road_dir[0] + 1, y + road_dir[1]) is not None:
			return False
		if IsExist(roads, x + road_dir[0], y + road_dir[1]) is not None:
			return False
		if IsExist(roads, x + road_dir[0] - 1, y + road_dir[1]) is not None:
			return False

	return True


def CreateLabyRoad_(roads, start_case, end_cond_func, dir_func):
	global directions, nbr_echec_road

	road = []
	roads.append(road)

	actual_case = start_case
	dir_iter_nbr = 0
	cpt_length = 0

	dir_order = dir_func()

	while end_cond_func(dir_iter_nbr, cpt_length):

		nbr_cp = random.randint(0, 4) + 1
		dir = directions[dir_order[dir_iter_nbr]]

		for x in range(nbr_cp):  # Next case

			next_case = [actual_case[0] + dir[0], actual_case[1] + dir[1]]

			if dir_iter_nbr == 0 and cpt_length < 50 and not IsExist(roads, actual_case[0], actual_case[1]):
				road.append(Case([actual_case[0], actual_case[1]]))

			if CanBeRoad(roads, next_case[0], next_case[1], dir) and cpt_length < 50:
				actual_case = next_case
				dir_iter_nbr = 0
				dir_order = dir_func()
				cpt_length += 1
			else:
				dir_iter_nbr += 1
				break

	if len(road) == 1 or len(road) == 0:
		roads.remove(road)
		nbr_echec_road += 1
	else:
		nbr_echec_road = 0


def CreateLabyRoad(roads, start_case, length_limit):
	def test_end_road(test_nbr, cpt):
		return test_nbr < 4 and cpt < length_limit

	def get_road_dir():
		return GetRandomDirectionOrder(4)

	return CreateLabyRoad_(roads, start_case, test_end_road, get_road_dir)


def CreateMainLabyRoad(roads, start_case, length_limit):
	def test_end_road(test_nbr, cpt):
		return test_nbr < 3 and cpt < length_limit

	def get_road_dir():
		return GetRandomDirectionOrder(3)  # Without WEST -> no step backwards

	return CreateLabyRoad_(roads, start_case, test_end_road, get_road_dir)


def CreateLaby(main_road_len, nbr_second_road, second_road_max_len, max_trap, min_trap):
	global main_road_start_dir
	laby = Laby()

	CreateMainLabyRoad(laby.roads, [0, 0], main_road_len)

	while len(laby.roads) - 1 < nbr_second_road and nbr_echec_road < 10:
		start_pos = GetRandomRoadStartPoint(laby.roads[0])
		CreateLabyRoad(laby.roads, start_pos, second_road_max_len)

	AddKey(laby)
	InitTraps(laby, max_trap, min_trap)
	InitExit(laby)

	return laby

# --------------------------------------- DEBUG ------------------------------------------------------------------------
def SaveLabydebugImg(labyCfg, path):
	case_trap = []

	min_x, min_y, max_x, max_y = GetLabyRect(labyCfg.roads)

	width, height = (max_x - min_x + 1) * 15, (max_y - min_y + 1) * 15
	img = Image.new('RGB', (width, height), (255, 255, 255))
	img_draw = ImageDraw.Draw(img)

	for road_idx, road in enumerate(labyCfg.roads):
		for case_idx, case in enumerate(road):
			road_color = (0, road_idx, 0)
			object_color = (0, road_idx, 0)
			if road_idx == 0:

				if case_idx == 0:
					object_color = PlayerColor

			if case.trap is not None:
				object_color = GetTrapColor(case.trap)
				case_trap.append(case.coord)

			if "key" in case.object:
				object_color = KeyColor

			if "exit" in case.object:
				object_color = ExitColor

			x = (case.coord[0] - min_x) * 15
			y = (case.coord[1] - min_y) * 15

			min_case = (x, y)
			max_case = (x + 14, y + 14)

			object_min_case = (x + 8, y + 1)
			object_max_case = (x + 13, y + 5)

			text_coord = [x + 1, y - 1]

			img_draw.rectangle([min_case, max_case], road_color)
			img_draw.rectangle([object_min_case, object_max_case], object_color)
			img_draw.text(text_coord, str(road_idx), (255, 255, 255))

	img.save(path)


def GetLabyRect(roads):
	min_x, min_y, max_x, max_y = 0, 0, 0, 0
	for road in roads:
		for case in road:
			if case.coord[0] < min_x:
				min_x = case.coord[0]
			if case.coord[1] < min_y:
				min_y = case.coord[1]
			if case.coord[0] > max_x:
				max_x = case.coord[0]
			if case.coord[1] > max_y:
				max_y = case.coord[1]

	return min_x, min_y, max_x, max_y


#------------------------------- SAVE AND LOAD LABY --------------------------------------------------------------------
def LoadLaby(path):
	newLaby = Laby()
	with open(path, "r") as file:
		nbr_road = int(file.readline())
		for r in range(nbr_road):
			nbr_case = int(file.readline())
			newLaby.roads.append([])
			for c in range(nbr_case):
				x = int(file.readline())
				y = int(file.readline())
				coord = [x, y]

				trap_type = file.readline().rstrip()
				if trap_type == "NoTrap":
					trap = None
				else:
					dexterity = int(file.readline())
					intelligence = int(file.readline())
					rapidity = int(file.readline())
					trap = Trap(trap_type, dexterity, intelligence, rapidity)

				objects = []
				nbr_object = int(file.readline())
				for object in range(nbr_object):
					new_object = file.readline().rstrip()
					objects.append(new_object)

				newCase = Case(coord)
				newCase.setCase(trap, objects)
				newLaby.roads[r].append(newCase)

	return newLaby


def SaveLaby(laby, path):
	with open(path, "w", encoding='utf-8') as file:
		file.write(str(len(laby.roads)) + '\n')
		for road in laby.roads:
			file.write(str(len(road)) + '\n')
			for case in road:
				file.write(str(case.coord[0]) + '\n')
				file.write(str(case.coord[1]) + '\n')

				if case.trap is None:
					file.write("NoTrap" + '\n')
				else:
					file.write(case.trap.type + '\n')
					file.write(str(case.trap.dexterity) + '\n')
					file.write(str(case.trap.intelligence) + '\n')
					file.write(str(case.trap.rapidity) + '\n')

				file.write(str(len(case.object)) + '\n')
				for obj in case.object:
					file.write(obj + '\n')


# if __name__ == "__main__":
# 	# laby1 = CreateLaby(50, random.randint(5, 8), 15)
# 	# SaveLabydebugImg(laby1, "C:/Users/movida-user/Desktop/Camille/test2img.png")
# 	# SaveLaby(laby1, "C:/Users/movida-user/Desktop/Camille/yolaby.txt")
# 	pass

