<img src="work/screenshots/menu.PNG"  width="904" height="569">

## The Game
_LABYRINTH 3D is a roguelike game._
You are lost in a huge dongeon. You have to find the key of the exit, in each of the levels which follow one another.
But be careful! There are some traps in your road...
You have to avoid from arrows, rock falls and iron stakes.
Save your health potion and try to survive as long as possible.

<img src="work/screenshots/iso_view.PNG"  width="904" height="569">
<img src="work/screenshots/top_view.PNG"  width="904" height="569">

## Tools
- Blender for the 3D models
- HARFANG 3D framework
- Python

## Attributions / Credits

### Sound
- [Underground ambiance](https://freesound.org/people/julius_galla/sounds/232685/?page=1#comment) by _**julius_galla**_
- [Key sound](https://freesound.org/people/EverHeat/sounds/205561/) by _**EverHeat**_
- [Rock fall](https://freesound.org/people/spookymodem/sounds/202098/) by _**spookymodem**_
- [Arrows](http://soundbible.com/1607-Throw-Knife.html) by _**Anonymous**_
- [Stake impact](http://soundbible.com/944-Stab.html) by _**Mike Koenig**_
- [Drink potion](https://freesound.org/people/Jamius/sounds/41529/) by _**Jamius**_
- [Open door](https://freesound.org/people/MWLANDI/sounds/85816/) by _**MWLANDI**_
- [Close door](https://freesound.org/people/nichols8917/sounds/339632/) by _**nichols8917**_
- [Characteristic choice](https://www.youtube.com/audiolibrary/soundeffects) (youtube audio library) in Sound Effects, named "Metal Thud"
- [Breathing](https://www.youtube.com/audiolibrary/soundeffects) (youtube audio library) in Sound Effects, named "Breathing deep fast"
- [Walking](https://www.youtube.com/audiolibrary/soundeffects) (youtube audio library) in Sound Effects, named "Walking fast on Concrete"

### Fonts
- [Stylish regular](https://fonts.google.com/specimen/Stylish) by _**AsiaSoft Inc**_
- [Montserrat Subrayada Bold](https://fonts.google.com/specimen/Montserrat+Subrayada) by _**Julieta Ulanovsky**_
- [Pangolin](https://fonts.google.com/specimen/Pangolin) by _**Kevin Burke**_

### Pictures
- [Health potion](https://adorabless.deviantart.com/art/Level-2-Health-Potion-OPEN-373505284) by _**adorabless**_
- [Heart](https://dribbble.com/shots/2589899-Low-Poly-Series-Red-Heart-Icon) by _**Charles Haitkin**_
- [Key](http://runescape.wikia.com/wiki/File:Gold_key_detail.png) by [_**RuneScape Game**_](https://www.runescape.com/l=2/community)
- [Wall texture](https://www.pinterest.fr/pin/333196072409494379/) and [Wall texture broken](https://www.pinterest.fr/pin/368732288213997940/) by _**League Of Legend CanGood**_
- [Column texture](http://polycount.com/discussion/132252/hand-painted-sculpted-texture) by _**Polynaut**_
- [Labyrinth icon](https://www.flaticon.com/free-icon/maze_158353#term=labyrinth&page=1&position=37) designed by from [_**Flaticon**_](https://www.flaticon.com/)

## How to play
- Download the latest version of [_**HARFANG(R))**_](https://www.harfang3d.com/downloads)
- Clone or download the project
- Run _main.py_ in the source directory